#ifndef TYPES_HH
#define TYPES_HH

#include <SDL2/SDL_rect.h>
#include <SDL2/SDL_video.h>
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

struct rectangle : SDL_Rect {
	rectangle() = default;
	rectangle(int ix, int iy, int iw, int ih) { 
		x = ix;
		y = iy;
		w = iw;
		h = ih;
	}
	rectangle(const rectangle& rec) { 
		x = rec.x;
		y = rec.y;
		w = rec.w;
		h = rec.h;
	}
};

struct window {
	SDL_Window* p = nullptr;

	window(SDL_Window* w) {
		if (p != nullptr)
			SDL_DestroyWindow(p);
		p = w;
	}
	~window() {
		SDL_DestroyWindow(p);
	}

	SDL_Window* operator*() {
		return p;
	}
};

struct renderer {
	SDL_Renderer* p = nullptr;

	renderer(SDL_Renderer* r) {
		if (p != nullptr)
			SDL_DestroyRenderer(p);
		p = r;
	}
	~renderer() {
		SDL_DestroyRenderer(p);
	}

	SDL_Renderer* operator*() {
		return p;
	}
};

struct surface {
	SDL_Surface* p = nullptr;

	surface() = default;
	surface(SDL_Surface* s) {
		if (p != nullptr)
			SDL_FreeSurface(p);
		p = s;
	}
	surface(surface&& s) {
		if (p != nullptr)
			SDL_FreeSurface(p);
		p = s.p;
		s.p = nullptr;
	}
	~surface() {
		SDL_FreeSurface(p);
	}

	surface& operator=(SDL_Surface* s) {
		if (p != nullptr)
			SDL_FreeSurface(p);
		p = s;
		return *this;
	}
	surface& operator=(surface&& s) {
		if (p != nullptr)
			SDL_FreeSurface(p);
		p = s.p;
		s.p = nullptr;
		return *this;
	}

	SDL_Surface* operator*() {
		return p;
	}
};

struct texture {
	SDL_Texture* p = nullptr;

	texture() = default;
	texture(SDL_Texture* t) {
		if (p != nullptr)
			SDL_DestroyTexture(p);
		p = t;
	}
	texture(texture&& t) {
		if (p != nullptr)
			SDL_DestroyTexture(p);
		p = t.p;
		t.p = nullptr;
	}
	~texture() {
		SDL_DestroyTexture(p);
	}

	texture& operator=(SDL_Texture* t) {
		if (p != nullptr)
			SDL_DestroyTexture(p);
		p = t;
		return *this;
	}
	texture& operator=(texture&& t) {
		if (p != nullptr)
			SDL_DestroyTexture(p);
		p = t.p;
		t.p = nullptr;
		return *this;
	}

	SDL_Texture* operator*() {
		return p;
	}
};

struct font {
	TTF_Font* p = nullptr;

	font() = default;
	font(TTF_Font* f) {
		if (p != nullptr)
			TTF_CloseFont(p);
		p = f;
	}
	font(font&& f) {
		if (p != nullptr)
			TTF_CloseFont(p);
		p = f.p;
		f.p = nullptr;
	}
	~font() {
		if (p != nullptr)
			TTF_CloseFont(p);
	}

	font& operator=(font&& f) {
		if (p != nullptr)
			TTF_CloseFont(p);
		p = f.p;
		f.p = nullptr;
		return *this;
	}

	TTF_Font* operator*() {
		return p;
	}
};

#endif
