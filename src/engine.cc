#include "engine.hh"
#include "rulegrid.hh"

inline
int abs(int a) { return a > 0 ? a : -a; }

void make_move(chessboard& board, int sx, int sy, int rx, int ry) {

	if (sx == rx && sy == ry)
		return;

	Piece temp = board(sx, sy);
	if (temp == none)
		return;

	std::array<bool, 64> legals = legal_moves(board, {sx, sy});
	if (!legals[ry*8 + rx])
		return;

	if ( (temp == king_w || temp == king_b) && abs(sx-rx) == 2 ) {
		if (rx == 2) {
			auto t_rook = board(0, ry);
			board(0, ry) = none;
			board(3, ry) = t_rook;
		} else {
			auto t_rook = board(7, ry);
			board(7, ry) = none;
			board(5, ry) = t_rook;
		}
	}


	board(sx, sy) = none;
	board(rx, ry) = temp;
	switch (temp) {
	case king_w:
		board.w_king_moved = true;
		break;
	case rook_w:
		if (sx == 0)
			board.w_l_rook_moved = true;
		else
			board.w_r_rook_moved = true;
		break;
	case king_b:
		board.b_king_moved = true;
		break;
	case rook_b:
		if (sx == 0)
			board.b_l_rook_moved = true;
		else
			board.b_r_rook_moved = true;
		break;
	default:
		break;
	}
}

std::array<bool, 64> legal_moves(const chessboard& board, ivec2d origin) {
	
	Piece temp = board(origin);
	bool white = temp < spacer;
	if (temp == none)
		return std::array<bool, 64>{};

	int  piece_type  = temp % spacer;
	rulegrid::rgrid rgrid(piece_type);

	int row = 7 - origin.y;
	int col = 7 - origin.x;

	/* Remove all illegal moves form rulegrid.
	 * piece_type 2 is rook and 5 is queen, so remove
	 * all horizontal and vertical moves that
	 * are illegal. */
	if (piece_type == 2 || piece_type == 5) {
		const int end = 8;
		const ivec2d pos{origin};
		ivec2d nray{};

		for (int dir = 0; dir < 4; ++dir) {
			switch (dir) {
			case 0: nray = {1, 0};
				break;
			case 1: nray = {0, 1};
				break;
			case 2: nray = {-1, 0};
				break;
			case 3: nray = {0, -1};
				break;
			}
			for (int beg = 1; beg < end; ++beg) {
				if (board(pos, nray*beg) != none) {
					for (++beg; beg < end; ++beg)
						rgrid[nray*beg] = '0';
					break;
				}
			}
		}
	}

	/* Remove all illegal moves form rulegrid.
	 * piece_type 4 is bishop and 5 is queen, so
	 * remove all diagonal moves that are illegal. */
	if (piece_type == 4 || piece_type == 5) {
		const int end = 8;
		const ivec2d pos{origin};
		ivec2d nray{};

		for (int dir = 0; dir < 4; ++dir) {
			switch (dir) {
			case 0: nray = {1, 1};
				break;
			case 1: nray = {1, -1};
				break;
			case 2: nray = {-1, 1};
				break;
			case 3: nray = {-1, -1};
				break;
			}
			for (int beg = 1; beg < end; ++beg) {
				if (board(pos, (nray*beg)) != none) {
					for (++beg; beg < end; ++beg) {
						rgrid[nray*beg] = '0';
					}
					break;
				}
			}
		}
	}

	/* First if-block constructs the result matrix for every piece
	 * save for pawns, since they need special handling, which is
	 * done int the else-clause. */
	std::array<bool, 64> res{};
	if (piece_type != 1) {
		for (int j = row, index = 0; j < row + 8; ++j) {
			for (int i = col; i < col + 8; ++i, ++index) {
				res[index] = rgrid[j*15 + i] == '1' ? true : false;
			}
		}
	}
	else if (piece_type == 1) {
		bool piece_white = temp < spacer;
		for (int j = row, index = 0; j < row + 8; ++j) {
			for (int i = col; i < col+8; ++i, ++index) {
				int p = j*15 + i;
				if (piece_white && (rgrid[p] == 'w' || rgrid[p] == 'e'))
					res[index] = true;
				else if (!piece_white && (rgrid[p] == 'b' || rgrid[p] == 'E'))
					res[index] = true;
				else
					res[index] = false;
			}
		}
		/* The abhorrentness of this next block is obvious.
		 * Every inner if has a guarding check to make sure we
		 * don't access  things out of bounds. The checks for
		 * black pieces have an explicit check for none, since
		 * the value of none is 0, and therefore included in the
		 * range for the checks for white pieces. */
		if (piece_white) {
			if ( (origin.x != 0 && origin.y != 0) &&
					!(board(origin+ivec2d{-1,-1}) > spacer) )
				res[ (origin.y-1)*8 + origin.x-1 ] = false;
			if ( (origin.x != 7 && origin.y != 0) &&
					!(board(origin+ivec2d{1,-1}) > spacer) )
				res[ (origin.y-1)*8 + origin.x+1 ] = false;
			if ( (origin.y != 7) && 
					board(origin+ivec2d{0,-1}) != none )
				res[ (origin.y-1)*8 + origin.x ] = false;
			if ( origin.y == 6 &&
					board(origin+ivec2d{0,-2}) == none )
				res[ (origin.y-2)*8 + origin.x ] = true;
		} else {
			if ( (origin.x != 0 && origin.y != 7) &&
					(!(board(origin+ivec2d{-1,1}) < spacer) ||
					(  board(origin+ivec2d{-1,1}) == none ) ) )
				res[ (origin.y+1)*8 + origin.x-1 ] = false;
			if ( (origin.x != 7 && origin.y != 7) &&
					(!(board(origin+ivec2d{1,1}) < spacer) ||
					(  board(origin+ivec2d{1,1}) == none ) ) )
				res[ (origin.y+1)*8 + origin.x+1 ] = false;
			if ( (origin.y != 0) && 
					board(origin+ivec2d{0,1}) != none )
				res[ (origin.y+1)*8 + origin.x ] = false;
			if ( origin.y == 1 &&
					board(origin+ivec2d{0,2}) == none )
				res[ (origin.y+2)*8 + origin.x ] = true;
		}

	}

	if (piece_type == 6) {
		bool piece_white = temp < spacer;
		if (piece_white && !board.w_king_moved) {
			bool clear_path = true;
			ivec2d nray{-1, 0};
			for (int i = 1; i < 4; ++i)
				if ( board( origin+(nray*i) ) != none )
					clear_path = false;
			if (clear_path && !board.w_l_rook_moved) 
				res[ origin.y*8 + origin.x-2 ] = true;

			clear_path = true;
			nray = {1, 0};
			for (int i = 1; i < 3; ++i)
				if ( board( origin+(nray*i) ) != none )
					clear_path = false;
			if (clear_path && !board.w_r_rook_moved) 
				res[ origin.y*8 + origin.x+2 ] = true;
		}
		else if (!board.b_king_moved) {
			bool clear_path = true;
			ivec2d nray{-1, 0};
			for (int i = 1; i < 4; ++i)
				if ( board( origin+(nray*i) ) != none )
					clear_path = false;
			if (clear_path && !board.b_l_rook_moved) 
				res[ origin.y*8 + origin.x-2 ] = true;

			clear_path = true;
			nray = {1, 0};
			for (int i = 1; i < 3; ++i)
				if ( board( origin+(nray*i) ) != none )
					clear_path = false;
			if (clear_path && !board.b_r_rook_moved) 
				res[ origin.y*8 + origin.x+2 ] = true;
		}
	}

	for (int i = 0; i < 64; ++i) {
		if (board[i] == none)
			;
		else if ((board[i] < spacer) == white)
			res[i] = false;
	}

	return res;
	
}

