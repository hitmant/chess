#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <SDL2/SDL.h>
#include <SDL2/SDL_rect.h>
#include <SDL2/SDL_video.h>
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "types.hh"
#include "engine.hh"
#include "render.hh"
#include "ivector.hh"

void ser_error(
		const std::string& msg, 
		decltype(SDL_GetError) f = SDL_GetError) {
	std::cerr << msg << f() << '\n';
	std::exit(1);
}

int main(int argc, char** argv) {
	//TODO: implement command line arguments
	
	if ( SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) ) 
		  ser_error("Failed to init SDL: ");
	atexit(SDL_Quit);

	if ( !IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG) ) 
		  ser_error("IMG_Init failed: ", IMG_GetError); 
	atexit(IMG_Quit);

	if ( TTF_Init() ) 
		  ser_error("TTF_Init failed: ", TTF_GetError);
	atexit(TTF_Quit);

	window main_window = SDL_CreateWindow(
			"Chess",
			SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED,
			522,
			552,
			0 );
	if (*main_window == nullptr)
		ser_error("Failed to create main_winow: ");

	renderer main_renderer = SDL_CreateRenderer(
			*main_window,
			-1,
			0 );
	if (*main_renderer == nullptr)
		ser_error("Failed to create main_renderer: ");
	SDL_SetRenderDrawBlendMode(*main_renderer, SDL_BLENDMODE_BLEND);

	std::map<Piece, texture> sprites{};
	const std::vector<std::string> filenames{
		"pawn_w.png", "rook_w.png", "knight_w.png", "bishop_w.png", 
		"queen_w.png", "king_w.png", "pawn_b.png", "rook_b.png", 
		"knight_b.png", "bishop_b.png", "queen_b.png", "king_b.png", 
	};
	for (int i = 1; const auto& name : filenames) {
		surface loading_surface = IMG_Load(("asset/image/" + name).c_str());
		texture temp = SDL_CreateTextureFromSurface(
				*main_renderer,
				*loading_surface );

		sprites[Piece(i++)] = std::move(temp);
		if ( i == spacer ) ++i;
	}

	font temp_font = TTF_OpenFont(
			"asset/font/anonymous-pro.regular.ttf",
			20 );
	if (*temp_font == nullptr)
		ser_error("Failed to open font: ", TTF_GetError);

	int width = 0;
	int height = 0;
	SDL_GetWindowSize(*main_window, &width, &height);
	init_render_engine(
			std::move(sprites), 
			std::move(temp_font), 
			width, 
			height );

	chessboard game_board{};
	chessboard old_board{nullptr};

	render_chess_board(main_renderer, game_board);
	std::pair<int, int> source{-1, -1};
	bool promotion  = false;
	bool white_turn = false;
	unsigned turn_counter = 0;
	ivec2d promotion_pos{-1, -1};
	SDL_Event e{};
	do {
		if (old_board != game_board && !promotion) {
			white_turn = !white_turn;
			old_board = game_board;
			turn_counter += white_turn ? 1 : 0;
			std::string turn_text{"Turn #"};
			turn_text += std::to_string(turn_counter);
			turn_text += white_turn ? ": White" : ": Black";
			set_text(turn_text);
			render_chess_board(main_renderer, game_board);
		}

		SDL_WaitEvent(&e);
		if (e.type == SDL_QUIT)
			break;
		else if (e.type == SDL_KEYUP && e.key.keysym.sym == SDLK_q)
			break;
		else if (e.type == SDL_KEYUP && e.key.keysym.sym == SDLK_d)
			pawn_promotion_screen(main_renderer);
		else if (e.type == SDL_KEYUP && e.key.keysym.sym == SDLK_r) {
			game_board = chessboard{};
			source = {-1, -1};
			render_chess_board(main_renderer, game_board);
		}
		else if (e.type == SDL_MOUSEBUTTONUP){
			if (exit_button_pressed(e.button.x, e.button.y))
				break;
			int x;
			int y;
			screen_to_board(e.button.x, e.button.y, x, y);
			if (promotion) {
				Piece promo = none;
				promo = pawn_promotion_select(e.button.x, e.button.y);
				promo = static_cast<Piece>(promo+(white_turn ? none : spacer));
				if (promo != none) {
					game_board(promotion_pos) = promo;
					render_chess_board(main_renderer, game_board);
					promotion = false;
					promotion_pos = {-1, -1};
				}
			} 
			else if (source.first == -1) {
				auto selected_piece = game_board(x, y);
				if (selected_piece == none)
					continue;
				else if (selected_piece > spacer &&  white_turn)
					continue;
				else if (selected_piece < spacer && !white_turn)
					continue;
				source = {x, y};
				highlight_tile(main_renderer, e.button.x, e.button.y);
			} 
			else {
				make_move(game_board, source.first, source.second, x, y);
				render_chess_board(main_renderer, game_board);
				int promo_row = white_turn ? 0 : 7;
				for (int i = 0; i < 8; ++i) {
					if (game_board(i, promo_row) == 
							(white_turn ? pawn_w : pawn_b)) {
						pawn_promotion_screen(main_renderer);
						promotion = true;
						promotion_pos = {i, promo_row};
					}
				}
				source = {-1, -1};
			}
		}
	} while (true);

	close_render_engine();

	return 0;
}
