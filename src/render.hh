#ifndef RENDER_HH
#define RENDER_HH

#include <map>

#include "types.hh"
#include "engine.hh"
#include "ivector.hh"

void init_render_engine(
		std::map<Piece, texture>&&, 
		font&&, 
		const int, 
		const int);

void close_render_engine();

void render_chess_board(renderer&, const chessboard&);

void screen_to_board(const int, const int, int&, int&);

void highlight_tile(renderer&, int, int);

void pawn_promotion_screen(renderer&);

void set_text(std::string, ivec2d = {20, 5});

void render_text(renderer&);

bool exit_button_pressed(int, int);

Piece pawn_promotion_select(int x, int y);

#endif
