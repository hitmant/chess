#ifndef ENGINE_HH
#define ENGINE_HH

#include <array>
#include <iostream>

#include "ivector.hh"

enum Piece : int {none = 0,
            pawn_w, rook_w, knight_w, bishop_w, queen_w, king_w, spacer,
            pawn_b, rook_b, knight_b, bishop_b, queen_b, king_b
};

constexpr std::array<Piece, 64> default_board{
	rook_b, knight_b, bishop_b, queen_b, king_b, bishop_b, knight_b, rook_b,
	pawn_b, pawn_b,   pawn_b,   pawn_b,  pawn_b, pawn_b,   pawn_b,   pawn_b, 
	none,   none,     none,     none,    none,   none,     none,     none, 
	none,   none,     none,     none,    none,   none,     none,     none, 
	none,   none,     none,     none,    none,   none,     none,     none, 
	none,   none,     none,     none,    none,   none,     none,     none, 
	pawn_w, pawn_w,   pawn_w,   pawn_w,  pawn_w, pawn_w,   pawn_w,   pawn_w, 
	rook_w, knight_w, bishop_w, queen_w, king_w, bishop_w, knight_w, rook_w,
};

struct chessboard {
	std::array<Piece, 64> board{default_board};
	bool b_king_moved = false;
	bool b_l_rook_moved = false;
	bool b_r_rook_moved = false;
	bool w_king_moved = false;
	bool w_l_rook_moved = false;
	bool w_r_rook_moved = false;

	chessboard() = default;
	explicit chessboard(auto) :
		board(std::array<Piece, 64>{}) {};


	Piece& operator[] (int i) {
		if (i >= 64 || i < 0)
			throw std::out_of_range("Index out of bounds for chessboard!");
		return board[i];
	}
	Piece operator[] (int i) const {
		if (i >= 64 || i < 0)
			throw std::out_of_range("Index out of bounds for chessboard!");
		return board[i];
	}
	Piece& operator() (int x, int y) {
		if (x < 0 || y < 0 || x > 8 || y > 8) {
			throw std::invalid_argument("Either x or y is out of bounds!");
		}
		return board[y*8 + x];
	}
	Piece& operator() (ivec2d v) {
		if (v.x < 0 || v.y < 0 || v.x > 8 || v.y > 8)
			throw std::invalid_argument("Either x or y is out of bounds!");
		return board[v.y*8 + v.x];
	}
	Piece operator() (ivec2d v) const {
		if (v.x < 0 || v.y < 0 || v.x > 8 || v.y > 8)
			throw std::invalid_argument("Either x or y is out of bounds!");
		return board[v.y*8 + v.x];
	}
	Piece operator() (int x, int y) const {
		if (x < 0 || y < 0 || x > 8 || y > 8) {
			throw std::invalid_argument("Either x or y is out of bounds!");
		}
		return board[y*8 + x];
	}
	Piece operator() (ivec2d pos, ivec2d offset) const {
		int res_x = pos.x + offset.x;
		int res_y = pos.y - offset.y;
		if (res_x < 0 || res_x < 0 || res_y > 8 || res_y > 8)
			return none;
		return board[res_y*8 + res_x];
	}
};

inline bool operator==(const chessboard& lhs, const chessboard& rhs) {
	for (int i = 0; i < 64; ++i)
		if (lhs.board[i] != rhs.board[i])
			return false;
	return true;
}

inline bool operator!=(const chessboard& lhs, const chessboard& rhs) {
	return !(lhs == rhs);
}

void make_move(chessboard&, int, int, int, int);

std::array<bool, 64> legal_moves(const chessboard&, ivec2d);

#endif
