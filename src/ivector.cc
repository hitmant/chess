#include "ivector.hh"


ivec2d operator+ (ivec2d lhs, ivec2d rhs) {
	return ivec2d(lhs.x + rhs.x, lhs.y + rhs.y);
}
ivec2d operator- (ivec2d lhs, ivec2d rhs) {
	return ivec2d(lhs.x - rhs.x, lhs.y - rhs.y);
}

ivec2d operator* (ivec2d vec, int scal) {
	return ivec2d(vec.x * scal, vec.y * scal);
}
ivec2d operator* (int scal, ivec2d vec) {
	return vec*scal;
}
