#ifndef IVECTOR_HH
#define IVECTOR_HH

struct ivec2d {
	int x;
	int y;

	ivec2d() = default;
	ivec2d(int a, int b) : x(a), y(b) {};
};

ivec2d operator+ (ivec2d lhs, ivec2d rhs);
//ivec2d operator- (ivec2d lhs, ivec2d rhs);

ivec2d operator* (ivec2d vec, int scal);
ivec2d operator* (int scal, ivec2d vec);

#endif
