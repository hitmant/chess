#include "render.hh"

/* Alignment constants for rendering function. */
constexpr int Tile_Size    = 64;
constexpr int Border_Size  = 5;
constexpr int Top_Space    = 30;
constexpr int Sprite_Size  = 60;
constexpr int Offset       = 4;

const rectangle Exit_Button{450, 5, 50, 25};

static int win_width;
static int win_height;
static font text_font;
static ivec2d text_pos;
static std::string text;
static std::map<Piece, texture> sprites;
static chessboard prev_board;

void init_render_engine(
		std::map<Piece, texture>&& input, 
		font&& t_font,
		const int width, 
		const int height) {
	sprites    = std::move(input);
	text_font  = std::move(t_font);
	win_width  = width;
	win_height = height;
	text       = " ";
	text_pos   = {0, 0};
}

void close_render_engine(){
	TTF_CloseFont(*text_font);
	text_font.p = nullptr;
}

// s-prefix: source, r-prefix: result
// Take two source ints and store board coordinates in result ints.
void screen_to_board(const int sx, const int sy, int& rx, int& ry) {
	rx = (sx - Border_Size) / Tile_Size;
	ry = (sy - Border_Size - Top_Space) / Tile_Size;
}

void set_text(std::string t, ivec2d pos) {
	text = t;
	text_pos = pos;
}

void render_text(renderer& ren) {
	SDL_Color text_color{255, 255, 255, 255};
	surface text_surface = TTF_RenderText_Solid(
			*text_font,
			text.c_str(),
			text_color );
	rectangle testr{
			text_pos.x, 
			text_pos.y, 
			text_surface.p->w, 
			text_surface.p->h};
	texture text_texture = SDL_CreateTextureFromSurface(
			*ren,
			*text_surface );
	SDL_RenderCopy(*ren, *text_texture, NULL, &testr);
}

void render_exit_button(renderer& ren) {
	SDL_SetRenderDrawColor(*ren, 230, 80, 80, 255);
	SDL_RenderFillRect(*ren, &Exit_Button);

	SDL_Color text_color{0, 0, 0, 255};
	surface text_surface = TTF_RenderText_Solid(
			*text_font,
			"EXIT",
			text_color );
	rectangle testr{
			Exit_Button.x+4, 
			Exit_Button.y+2, 
			text_surface.p->w, 
			text_surface.p->h};
	texture text_texture = SDL_CreateTextureFromSurface(
			*ren,
			*text_surface );
	SDL_RenderCopy(*ren, *text_texture, NULL, &testr);
}

void render_chess_board(renderer& ren, const chessboard& board = prev_board) {

	SDL_SetRenderDrawColor(*ren, 0, 0, 0, 255);
	SDL_RenderClear(*ren);


	rectangle rect{Border_Size, Border_Size+Top_Space, Tile_Size, Tile_Size};
	rectangle piece{0, 0, Sprite_Size, Sprite_Size};

	for (int i = 0; i < 8; ++i) {
		for (int j = 0; j < 8; ++j) {
			if ( ( (i % 2) + (j % 2) ) % 2 != 1 )
				SDL_SetRenderDrawColor(*ren, 200, 200, 200, 255);
			else
				SDL_SetRenderDrawColor(*ren, 100, 100, 100, 255);
			
			SDL_RenderFillRect(*ren, &rect);
			rect.x += Tile_Size;

			if (board(j, i) != none) {
				piece.x = j*Tile_Size + Offset;
				piece.y = i*Tile_Size + Offset + Top_Space;
				Piece t = board(j, i);
				texture& img = sprites.at(t);

				SDL_RenderCopy(*ren, *img, NULL, &piece);
			}
		}
		rect.x  = Border_Size;
		rect.y += Tile_Size;
	}

	render_text(ren);
	render_exit_button(ren);

	SDL_RenderPresent(*ren);
	prev_board = board;
}

void highlight_tile(renderer& ren, int x, int y) {
	if ( x < 0 || y < 0 || x > win_width || y > win_height )
		return;

	x = (x - Border_Size) / Tile_Size;
	y = (y - Border_Size - Top_Space) / Tile_Size;

	render_chess_board(ren);
	rectangle hilight{
		x*Tile_Size + Border_Size, 
		y*Tile_Size + Border_Size + Top_Space, 
		Tile_Size, 
		Tile_Size };

	SDL_SetRenderDrawColor(*ren, 20, 200, 20, 126);
	SDL_RenderFillRect(*ren, &hilight);

	std::array<bool, 64> legal_grid = legal_moves(prev_board, ivec2d{x, y});
	for (int tile = 0; auto b : legal_grid) {
		if (b) {
			rectangle hi_legal {
				(tile % 8)*Tile_Size + Border_Size,
				(tile / 8)*Tile_Size + Border_Size + Top_Space,
				Tile_Size,
				Tile_Size };
			SDL_SetRenderDrawColor(*ren, 150, 150, 20, 100);
			SDL_RenderFillRect(*ren, &hi_legal);
		}
		++tile;
	}
	SDL_RenderPresent(*ren);

}

void pawn_promotion_screen(renderer& ren) {

	rectangle background{
			win_width/2 - 2*Tile_Size - Border_Size,
			win_height/2 - Tile_Size/2 - Border_Size + Top_Space,
			4*Tile_Size + 2*Border_Size,
			Tile_Size + 2*Border_Size };
	SDL_SetRenderDrawColor(*ren, 50, 50, 50, 255);
	SDL_RenderFillRect(*ren, &background);

	rectangle piece{
			win_width/2 - 2*Tile_Size + (Tile_Size-Sprite_Size)/2,
			win_height/2 - Sprite_Size/2 + Top_Space,
			Sprite_Size,
			Sprite_Size	};
	const std::array<Piece, 4> pieces{rook_w, knight_w, bishop_w, queen_w};
	for (const auto p : pieces) {
		texture& img = sprites.at(p);
		SDL_RenderCopy(*ren, *img, NULL, &piece);
		piece.x += Tile_Size;
	}

	SDL_RenderPresent(*ren);
}

bool exit_button_pressed(int x, int y) {
	if ( x > Exit_Button.x && x < Exit_Button.x + Exit_Button.w &&
			y > Exit_Button.y && y < Exit_Button.y + Exit_Button.h ) {
		return true;
	}
	return false;
}

Piece pawn_promotion_select(int x, int y) {
	if ( y < win_height/2 - Sprite_Size/2 + Top_Space || 
			y > win_height/2 + Sprite_Size/2 + Top_Space )
		return none;
	else if ( x < win_width/2 - 2*Tile_Size + (Tile_Size-Sprite_Size)/2 || 
			x > win_width/2 + 2*Tile_Size - (Tile_Size-Sprite_Size)/2 )
		return none;
	else {
		x -= win_width/2 - 2*Tile_Size + (Tile_Size-Sprite_Size)/2;
		if (x < Tile_Size)
			return rook_w;
		else if (x < 2*Tile_Size)
			return knight_w;
		else if (x < 3*Tile_Size)
			return bishop_w;
		else 
			return queen_w;
	}
	return king_b;
}
