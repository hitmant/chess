# Chess Makefile
SHELL    := /bin/bash
CXX      := g++
CXXFLAGS := -Wall $(shell sdl2-config --cflags) -std=c++20
CXXLIBS  := $(shell sdl2-config --libs) -lSDL2_image -lSDL2_ttf

BRANCH := $(shell git branch 2>/dev/null | grep '^*' | colrm 1 2)
ifeq ($(BRANCH),master)
CXXFLAGS += -O3
else
CXXFLAGS += -Og -g
endif

SRCDIR := src
OBJDIR := obj

vpath %.cc $(SRCDIR)
vpath %.hh $(SRCDIR)

TARGET  := chess
SRCS_CC := main.cc types.cc render.cc engine.cc ivector.cc
SRCS    := $(SRCS_CC)
OBJS    := $(addprefix $(OBJDIR)/, $(SRCS:.cc=.o))


$(TARGET) : $(SRCS) $(OBJS)
	$(CXX) $(CXXFLAGS) $(CXXLIBS) -o $@ $(filter %.o, $^)

$(OBJDIR)/main.o   : types.hh engine.hh render.hh
$(OBJDIR)/types.o  : types.hh
$(OBJDIR)/render.o : render.hh
$(OBJDIR)/engine.o : engine.hh ivector.hh rulegrid.hh
$(OBJDIR)/ivector.o: ivector.hh

$(OBJDIR)/%.o : %.cc | $(OBJDIR)
	$(CXX) $(CXXFLAGS) -c -o $@ $<

$(OBJDIR) :
	mkdir $(OBJDIR)

.PHONY: clean
clean :
	rm -drf $(OBJDIR)
	rm -f $(TARGET)
