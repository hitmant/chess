# Chess

[![Version](https://img.shields.io/badge/Version-0.9.0-yellow.svg)](https://shields.io/)
[![Licence](https://img.shields.io/badge/Licence-GPLv3.0-blue.svg)](https://shields.io/)

## About:
A simple chess game. It features normal chess rules and nothing more.
Checkmates aren't being checked automatically, as illegal moves which
will put the player's king in danger are still concidered legal by the
engine. This should get fixed in the 1.0 release.

## Building:
This program is built using GNU Make 4.3, GCC 10.1.0 and SDL 2.0.12.

To build this program simply run `make` and you get a runnable
executable. This executable however is not portable, as it depends
on the assets-folder for the sprites and font.

## Licence:
Chess
Copyright (C) 2020  hitmant

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
